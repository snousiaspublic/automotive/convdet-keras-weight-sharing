# Project: squeezeDetOnKeras
# Filename: squeezeDet
# Author: Christopher Ehmann
# Date: 28.11.17
# Organisation: searchInk
# Email: christopher@searchink.com


from keras.models import Model
from keras.layers import Input, MaxPool2D, Conv2D, Dropout, concatenate, Reshape, Lambda, AveragePooling2D, Activation, \
    BatchNormalization, add, ZeroPadding2D, MaxPooling2D, ZeroPadding1D, ReLU,LeakyReLU
from keras.engine.topology import Layer
from keras.constraints import max_norm
from keras import backend as K
from keras.initializers import TruncatedNormal,glorot_normal
from keras.regularizers import l2
import main.utils.utils as utils
import numpy as np
import tensorflow.compat.v1 as tf


class Added_Weights(Layer):
    def __init__(self, name='kernel',trainable=True, **kwargs):
        super(Added_Weights, self).__init__(**kwargs)
        self.name = name
        self.trainable=trainable

    def build(self, input_shape, ):
        # Create a trainable weight variable for this layer.
        self.bias = self.add_weight(name=self.name,
                                      shape=(input_shape[3],),
                                      initializer='ones',  # TODO: Choose your initializer
                                      trainable=self.trainable
                                      )
        super(Added_Weights, self).build(input_shape)

    def call(self, x, **kwargs):
        # Implicit broadcasting occurs here.
        # Shape x: (BATCH_SIZE, N, M)
        # Shape kernel: (N, M)
        # Shape output: (BATCH_SIZE, N, M)
        # return tf.nn.bias_add(x, self.kernel)
        return K.bias_add(x,self.bias)

    def compute_output_shape(self, input_shape):
        return input_shape








# class that wraps config and model
# [False , False, False, False, True]
class VGG16DetBiasV2():
    # initialize model from config file
    def __init__(self, config,trainablePerBlock = [False , False, True, True, True]):
        """Init of SqueezeDet Class
        Arguments:
            config {[type]} -- dict containing hyperparameters for network building
        """
        # hyperparameter config file
        self.config = config
        self.bn_axis = 3
        self.doTransfer = False
        # self.kernel_initializer = 'he_normal'
        self.trainablePerBlock = trainablePerBlock


        # create Keras model
        self.model = self._create_model()

    # creates keras model
    def _create_model(self):
        """
        #builds the Keras model from config
        #return: squeezeDet in Keras
        """


        input_layer = Input(
            shape=(int(self.config.IMAGE_HEIGHT), int(self.config.IMAGE_WIDTH), int(self.config.N_CHANNELS)),
            name="input")

        # x = BatchNormalization(axis=self.bn_axis)(input_layer)


        # Block 1
        x = Conv2D(filters=64,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   trainable= self.trainablePerBlock[0],
                   activation=None,
                   padding='same',
                   name='block1_conv1',
                   use_bias=False)(input_layer)
        x = Added_Weights(name='block1_conv1/block1_conv1/bias',trainable=self.trainablePerBlock[0])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=64,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   trainable=self.trainablePerBlock[0],
                   padding='same',
                   name='block1_conv2',
                   use_bias=False)(x)
        x = Added_Weights(name='block1_conv2/block1_conv2/bias',trainable=self.trainablePerBlock[0])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x)

        # Block 2
        x = Conv2D(filters=128,
                   trainable=self.trainablePerBlock[1],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block2_conv1',
                   use_bias=False)(x)
        x = Added_Weights(name='block2_conv1/block2_conv1/bias',trainable=self.trainablePerBlock[1])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=128,
                   trainable=self.trainablePerBlock[1],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block2_conv2',
                   use_bias=False)(x)
        x = Added_Weights(name='block2_conv2/block2_conv2/bias',trainable=self.trainablePerBlock[1])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x)

        # Block 3
        x = Conv2D(filters=256,
                   trainable=self.trainablePerBlock[2],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block3_conv1',
                   use_bias=False)(x)
        x = Added_Weights(name='block3_conv1/block3_conv1/bias',trainable=self.trainablePerBlock[2])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=256,
                   trainable=self.trainablePerBlock[2],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block3_conv2',
                   use_bias=False)(x)
        x = Added_Weights(name='block3_conv2/block3_conv2/bias',trainable=self.trainablePerBlock[2])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=256,
                   trainable=self.trainablePerBlock[2],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block3_conv3',
                   use_bias=False)(x)
        x = Added_Weights(name='block3_conv3/block3_conv3/bias',trainable=self.trainablePerBlock[2])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x)

        # Block 4
        x = Conv2D(filters=512,
                   trainable=self.trainablePerBlock[3],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block4_conv1',
                   use_bias=False)(x)
        x = Added_Weights(name='block4_conv1/block4_conv1/bias',trainable=self.trainablePerBlock[3])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=512,
                   trainable=self.trainablePerBlock[3],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block4_conv2',
                   use_bias=False)(x)
        x = Added_Weights(name='block4_conv2/block4_conv2/bias',trainable=self.trainablePerBlock[3])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=512,
                   trainable=self.trainablePerBlock[3],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block4_conv3',
                   use_bias=False)(x)
        x = Added_Weights(name='block4_conv3/block4_conv3/bias',trainable=self.trainablePerBlock[3])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x)

        # Block 5
        x = Conv2D(filters=512,
                   trainable=self.trainablePerBlock[4],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block5_conv1',
                   use_bias=False)(x)
        x = Added_Weights(name='block5_conv1/block5_conv1/bias',trainable=self.trainablePerBlock[4])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=512,
                   trainable=self.trainablePerBlock[4],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block5_conv2',
                   use_bias=False)(x)
        x = Added_Weights(name='block5_conv2/block5_conv2/bias',trainable=self.trainablePerBlock[4])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        x = Conv2D(filters=512,
                   trainable=self.trainablePerBlock[4],
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   # kernel_constraint=max_norm(2.),
                   kernel_size=(3, 3),
                   activation=None,
                   padding='same',
                   name='block5_conv3',
                   use_bias=False)(x)
        x = Added_Weights(name='block5_conv3/block5_conv3/bias',trainable=self.trainablePerBlock[4])(x)
        # x = LeakyReLU(0.2)(x)
        # x = BatchNormalization(axis=3)(x)
        x = ReLU()(x)


        # x = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)

        # if include_top:
        #     # Classification block
        #     x = Flatten(name='flatten')(x)
        #     x = Dense(4096, activation='relu', name='fc1')(x)
        #     x = Dense(4096, activation='relu', name='fc2')(x)
        #     x = Dense(1000, activation='softmax', name='predictions')(x)

        # x = ZeroPadding2D(padding=(3, 3), name='conv1_pad')(input_layer)
        #
        # x = Conv2D(filters=64,
        #            kernel_size=(7, 7),
        #            strides=(2, 2),
        #            padding='valid',
        #            use_bias=True,
        #            kernel_initializer=TruncatedNormal(stddev=0.001),
        #            name='conv1',
        #            trainable=not doTransfer)(x)
        # x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
        # x = Activation('relu')(x)
        # x = ZeroPadding2D(padding=(1, 1), name='pool1_pad')(x)
        # x = MaxPooling2D((3, 3), strides=(2, 2))(x)
        #
        #
        #
        #
        #
        # x = self._conv_block(x, 3, [64, 64, 256], 2, 'a', (1, 1),trainable=True,relu=False)
        # x = self._identity_block(x, 3, [64, 64, 256], 2, 'b',trainable=True)
        # x = self._identity_block(x, 3, [64, 64, 256], 2, 'c',trainable=True)
        #
        # x = self._conv_block(x, 3, [128, 128, 512], 3, 'a',trainable=True,relu=False)
        # x = self._identity_block(x, 3, [128, 128, 512], 3, 'b',trainable=True)
        # x = self._identity_block(x, 3, [128, 128, 512], 3, 'c',trainable=True)
        # x = self._identity_block(x, 3, [128, 128, 512], 3, 'd',trainable=True)
        #
        # x = self._conv_block(x, 3, [256, 256, 1024], 4, 'a',trainable=True,relu=False)
        # x = self._identity_block(x, 3, [256, 256, 1024], 4, 'b',trainable=True)
        # x = self._identity_block(x, 3, [256, 256, 1024], 4, 'c',trainable=True)
        # x = self._identity_block(x, 3, [256, 256, 1024], 4, 'd',trainable=True)
        # x = self._identity_block(x, 3, [256, 256, 1024], 4, 'e',trainable=True)
        # x = self._identity_block(x, 3, [256, 256, 1024], 4, 'f',trainable=True)
        # x = self._identity_block(x, 3, [256, 256, 1024], 4, 'h',trainable=True)

        # x = self._conv_block(x, 3, [512, 512, 2048], 5, 'a')
        # x = self._identity_block(x, 3, [512, 512, 2048], 5, 'b')
        # x = self._identity_block(x, 3, [512, 512, 2048], 5, 'c')

        dropout11 = Dropout(rate=self.config.KEEP_PROB, name='drop11')(x)
        # compute the number of output nodes from number of anchors, classes, confidence score and bounding box corners
        num_output = self.config.ANCHOR_PER_GRID * (self.config.CLASSES + 1 + 4)

        preds = Conv2D(
            name='conv12',
            filters=num_output,
            kernel_size=(3, 3),
            strides=(1, 1),
            activation=None,
            padding="SAME",
            use_bias=True,
            #kernel_initializer=glorot_normal(),
            kernel_initializer=TruncatedNormal(stddev=0.0001),
            kernel_regularizer=l2(self.config.WEIGHT_DECAY),
            trainable=True
            #
        )(dropout11)
        #preds = BatchNormalization(axis=3)(preds)

        # reshape
        pred_reshaped = Reshape((self.config.ANCHORS, -1))(preds)
        # pad for loss function so y_pred and y_true have the same dimensions, wrap in lambda layer
        pred_padded = Lambda(self._pad)(pred_reshaped)
        # pred_padded=ZeroPadding2D(padding=(3,2))(pred_reshaped)
        model = Model(inputs=input_layer, outputs=pred_padded)
        return model

    def _identity_block(self, input_tensor, kernel_size, filters, stage, block, bn_axis=3,
                        trainable=True,
                        conv_with_bias=False):
        """The identity block is the block that has no conv layer at shortcut.

        # Arguments
            input_tensor: input tensor
            kernel_size: default 3, the kernel size of
                middle conv layer at main path
            filters: list of integers, the filters of 3 conv layer at main path
            stage: integer, current stage label, used for generating layer names
            block: 'a','b'..., current block label, used for generating layer names

        # Returns
            Output tensor for the block.
        """
        filters1, filters2, filters3 = filters
        # if backend.image_data_format() == 'channels_last':
        #     bn_axis = 3
        # else:
        #     bn_axis = 1
        conv_name_base = 'res' + str(stage) + block + '_branch'
        bn_name_base = 'bn' + str(stage) + block + '_branch'

        x = Conv2D(filters=filters1,
                   kernel_size=(1, 1),
                   use_bias=conv_with_bias,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   kernel_constraint=max_norm(2.),
                   name=conv_name_base + '2a',
                   trainable=trainable)(input_tensor)

        x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
        x = Activation('relu')(x)

        x = Conv2D(filters=filters2,
                   kernel_size=kernel_size,
                   use_bias=conv_with_bias,
                   padding='same',
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   kernel_constraint=max_norm(2.),
                   name=conv_name_base + '2b',
                   trainable=trainable)(x)

        x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
        x = Activation('relu')(x)

        x = Conv2D(filters=filters3, kernel_size=(1, 1),
                   use_bias=conv_with_bias,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   kernel_constraint=max_norm(2.),
                   name=conv_name_base + '2c',
                   trainable=trainable)(x)
        # x = Added_Weights()(x)

        x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)
        x = add([x, input_tensor])
        x = Activation('relu')(x)
        return x

    def _conv_block(self,
                    input_tensor,
                    kernel_size,
                    filters,
                    stage,
                    block,
                    strides=(2, 2),
                    bn_axis=3,
                    trainable=True,
                    conv_with_bias=False,
                    relu=True):
        """A block that has a conv layer at shortcut.

        # Arguments
            input_tensor: input tensor
            kernel_size: default 3, the kernel size of
                middle conv layer at main path
            filters: list of integers, the filters of 3 conv layer at main path
            stage: integer, current stage label, used for generating layer names
            block: 'a','b'..., current block label, used for generating layer names
            strides: Strides for the first conv layer in the block.

        # Returns
            Output tensor for the block.

        Note that from stage 3,
        the first conv layer at main path is with strides=(2, 2)
        And the shortcut should have strides=(2, 2) as well
        """
        filters1, filters2, filters3 = filters
        # if backend.image_data_format() == 'channels_last':
        #     bn_axis = 3
        # else:
        #     bn_axis = 1
        conv_name_base = 'res' + str(stage) + block + '_branch'
        bn_name_base = 'bn' + str(stage) + block + '_branch'

        x = Conv2D(filters=filters1,
                   kernel_size=(1, 1),
                   strides=strides,
                   use_bias=conv_with_bias,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   kernel_constraint=max_norm(2.),
                   name=conv_name_base + '2a',
                   trainable=trainable)(input_tensor)
        # x = Added_Weights()(x)

        x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
        if relu:
            x = Activation('relu')(x)

        x = Conv2D(filters=filters2,
                   kernel_size=kernel_size,
                   padding='same',
                   use_bias=conv_with_bias,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   kernel_constraint=max_norm(2.),
                   name=conv_name_base + '2b',
                   trainable=trainable)(x)
        # x = Added_Weights()(x)

        x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
        if relu:
            x = Activation('relu')(x)

        x = Conv2D(filters=filters3,
                   kernel_size=(1, 1),
                   use_bias=conv_with_bias,
                   kernel_initializer=TruncatedNormal(stddev=0.001),
                   kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                   kernel_constraint=max_norm(2.),
                   name=conv_name_base + '2c',
                   trainable=trainable)(x)
        # x = Added_Weights()(x)

        x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

        shortcut = Conv2D(filters=filters3,
                          kernel_size=(1, 1),
                          strides=strides,
                          use_bias=conv_with_bias,
                          kernel_initializer=TruncatedNormal(stddev=0.001),
                          kernel_regularizer=l2(self.config.WEIGHT_DECAY),
                          kernel_constraint=max_norm(2.),
                          name=conv_name_base + '1',
                          trainable=trainable)(input_tensor)
        # shortcut = Added_Weights()(shortcut)

        shortcut = BatchNormalization(
            axis=bn_axis, name=bn_name_base + '1')(shortcut)
        x = add([x, shortcut])
        if relu:
            x = Activation('relu')(x)
        return x

    # wrapper for padding, written in tensorflow. If you want to change to theano you need to rewrite this!
    def _pad(self, input):
        """
        pads the network output so y_pred and y_true have the same dimensions
        :param input: previous layer
        :return: layer, last dimensions padded for 4
        """

        # pad = K.placeholder( (None,self.config.ANCHORS, 4))
        # pad = np.zeros ((self.config.BATCH_SIZE,self.config.ANCHORS, 4))
        # return K.concatenate( [input, pad], axis=-1)

        ###
        padding = np.zeros((3, 2))
        padding[2, 1] = 4
        return tf.pad(input, padding, "CONSTANT")

    # loss function to optimize
    def loss(self, y_true, y_pred):
        """
        squeezeDet loss function for object detection and classification
        :param y_true: ground truth with shape [batchsize, #anchors, classes+8+labels]
        :param y_pred:
        :return: a tensor of the total loss
        """

        # handle for config
        mc = self.config

        # slice y_true
        input_mask = y_true[:, :, 0]
        input_mask = K.expand_dims(input_mask, axis=-1)
        box_input = y_true[:, :, 1:5]
        box_delta_input = y_true[:, :, 5:9]
        labels = y_true[:, :, 9:]

        # number of objects. Used to normalize bbox and classification loss
        num_objects = K.sum(input_mask)

        # before computing the losses we need to slice the network outputs
        pred_class_probs, pred_conf, pred_box_delta = utils.slice_predictions(y_pred, mc)

        # compute boxes
        det_boxes = utils.boxes_from_deltas(pred_box_delta, mc)

        # again unstack is not avaible in pure keras backend
        unstacked_boxes_pred = []
        unstacked_boxes_input = []

        for i in range(4):
            unstacked_boxes_pred.append(det_boxes[:, :, i])
            unstacked_boxes_input.append(box_input[:, :, i])

        # compute the ious
        ious = utils.tensor_iou(utils.bbox_transform(unstacked_boxes_pred),
                                utils.bbox_transform(unstacked_boxes_input),
                                input_mask,
                                mc
                                )

        # compute class loss,add a small value into log to prevent blowing up
        class_loss = K.sum(labels * (-K.log(pred_class_probs + mc.EPSILON))
                           + (1 - labels) * (-K.log(1 - pred_class_probs + mc.EPSILON))
                           * input_mask * mc.LOSS_COEF_CLASS) / num_objects

        # bounding box loss
        bbox_loss = (K.sum(mc.LOSS_COEF_BBOX * K.square(input_mask * (pred_box_delta - box_delta_input))) / num_objects)

        # reshape input for correct broadcasting
        input_mask = K.reshape(input_mask, [mc.BATCH_SIZE, mc.ANCHORS])

        # confidence score loss
        conf_loss = K.mean(
            K.sum(
                K.square((ious - pred_conf))
                * (input_mask * mc.LOSS_COEF_CONF_POS / num_objects
                   + (1 - input_mask) * mc.LOSS_COEF_CONF_NEG / (mc.ANCHORS - num_objects)),
                axis=[1]
            ),
        )

        # add above losses
        total_loss = class_loss + conf_loss + bbox_loss

        return total_loss

    # the sublosses, to be used as metrics during training

    def bbox_loss(self, y_true, y_pred):
        """
        squeezeDet loss function for object detection and classification
        :param y_true: ground truth with shape [batchsize, #anchors, classes+8+labels]
        :param y_pred:
        :return: a tensor of the bbox loss
        """

        # handle for config
        mc = self.config

        # calculate non padded entries
        n_outputs = mc.CLASSES + 1 + 4

        # slice and reshape network output
        y_pred = y_pred[:, :, 0:n_outputs]
        y_pred = K.reshape(y_pred, (mc.BATCH_SIZE, mc.N_ANCHORS_HEIGHT, mc.N_ANCHORS_WIDTH, -1))

        # slice y_true
        input_mask = y_true[:, :, 0]
        input_mask = K.expand_dims(input_mask, axis=-1)
        box_delta_input = y_true[:, :, 5:9]

        # number of objects. Used to normalize bbox and classification loss
        num_objects = K.sum(input_mask)

        # before computing the losses we need to slice the network outputs

        # number of class probabilities, n classes for each anchor
        num_class_probs = mc.ANCHOR_PER_GRID * mc.CLASSES

        # number of confidence scores, one for each anchor + class probs
        num_confidence_scores = mc.ANCHOR_PER_GRID + num_class_probs

        # slice the confidence scores and put them trough a sigmoid for probabilities
        pred_conf = K.sigmoid(
            K.reshape(
                y_pred[:, :, :, num_class_probs:num_confidence_scores],
                [mc.BATCH_SIZE, mc.ANCHORS]
            )
        )

        # slice remaining bounding box_deltas
        pred_box_delta = K.reshape(
            y_pred[:, :, :, num_confidence_scores:],
            [mc.BATCH_SIZE, mc.ANCHORS, 4]
        )

        # cross-entropy: q * -log(p) + (1-q) * -log(1-p)
        # add a small value into log to prevent blowing up

        # bounding box loss
        bbox_loss = (K.sum(mc.LOSS_COEF_BBOX * K.square(input_mask * (pred_box_delta - box_delta_input))) / num_objects)

        return bbox_loss

    def conf_loss(self, y_true, y_pred):
        """
        squeezeDet loss function for object detection and classification
        :param y_true: ground truth with shape [batchsize, #anchors, classes+8+labels]
        :param y_pred:
        :return: a tensor of the conf loss
        """

        # handle for config
        mc = self.config

        # calculate non padded entries
        n_outputs = mc.CLASSES + 1 + 4

        # slice and reshape network output
        y_pred = y_pred[:, :, 0:n_outputs]
        y_pred = K.reshape(y_pred, (mc.BATCH_SIZE, mc.N_ANCHORS_HEIGHT, mc.N_ANCHORS_WIDTH, -1))

        # slice y_true
        input_mask = y_true[:, :, 0]
        input_mask = K.expand_dims(input_mask, axis=-1)
        box_input = y_true[:, :, 1:5]

        # number of objects. Used to normalize bbox and classification loss
        num_objects = K.sum(input_mask)

        # before computing the losses we need to slice the network outputs

        # number of class probabilities, n classes for each anchor
        num_class_probs = mc.ANCHOR_PER_GRID * mc.CLASSES

        # number of confidence scores, one for each anchor + class probs
        num_confidence_scores = mc.ANCHOR_PER_GRID + num_class_probs

        # slice the confidence scores and put them trough a sigmoid for probabilities
        pred_conf = K.sigmoid(
            K.reshape(
                y_pred[:, :, :, num_class_probs:num_confidence_scores],
                [mc.BATCH_SIZE, mc.ANCHORS]
            )
        )

        # slice remaining bounding box_deltas
        pred_box_delta = K.reshape(
            y_pred[:, :, :, num_confidence_scores:],
            [mc.BATCH_SIZE, mc.ANCHORS, 4]
        )

        # compute boxes
        det_boxes = utils.boxes_from_deltas(pred_box_delta, mc)

        # again unstack is not avaible in pure keras backend
        unstacked_boxes_pred = []
        unstacked_boxes_input = []

        for i in range(4):
            unstacked_boxes_pred.append(det_boxes[:, :, i])
            unstacked_boxes_input.append(box_input[:, :, i])

        # compute the ious
        ious = utils.tensor_iou(utils.bbox_transform(unstacked_boxes_pred),
                                utils.bbox_transform(unstacked_boxes_input),
                                input_mask,
                                mc
                                )

        # reshape input for correct broadcasting
        input_mask = K.reshape(input_mask, [mc.BATCH_SIZE, mc.ANCHORS])

        # confidence score loss
        conf_loss = K.mean(
            K.sum(
                K.square((ious - pred_conf))
                * (input_mask * mc.LOSS_COEF_CONF_POS / num_objects
                   + (1 - input_mask) * mc.LOSS_COEF_CONF_NEG / (mc.ANCHORS - num_objects)),
                axis=[1]
            ),
        )

        return conf_loss

    def class_loss(self, y_true, y_pred):
        """
        squeezeDet loss function for object detection and classification
        :param y_true: ground truth with shape [batchsize, #anchors, classes+8+labels]
        :param y_pred:
        :return: a tensor of the class loss
        """

        # handle for config
        mc = self.config

        # calculate non padded entries
        n_outputs = mc.CLASSES + 1 + 4

        # slice and reshape network output
        y_pred = y_pred[:, :, 0:n_outputs]
        y_pred = K.reshape(y_pred, (mc.BATCH_SIZE, mc.N_ANCHORS_HEIGHT, mc.N_ANCHORS_WIDTH, -1))

        # slice y_true
        input_mask = y_true[:, :, 0]
        input_mask = K.expand_dims(input_mask, axis=-1)
        labels = y_true[:, :, 9:]

        # number of objects. Used to normalize bbox and classification loss
        num_objects = K.sum(input_mask)

        # before computing the losses we need to slice the network outputs

        # number of class probabilities, n classes for each anchor
        num_class_probs = mc.ANCHOR_PER_GRID * mc.CLASSES

        # slice pred tensor to extract class pred scores and then normalize them
        pred_class_probs = K.reshape(
            K.softmax(
                K.reshape(
                    y_pred[:, :, :, :num_class_probs],
                    [-1, mc.CLASSES]
                )
            ),
            [mc.BATCH_SIZE, mc.ANCHORS, mc.CLASSES],
        )

        # cross-entropy: q * -log(p) + (1-q) * -log(1-p)
        # add a small value into log to prevent blowing up

        # compute class loss
        class_loss = K.sum((labels * (-K.log(pred_class_probs + mc.EPSILON))
                            + (1 - labels) * (-K.log(1 - pred_class_probs + mc.EPSILON)))
                           * input_mask * mc.LOSS_COEF_CLASS) / num_objects

        return class_loss

    # loss function again, used for metrics to show loss without regularization cost, just of copy of the original loss
    def loss_without_regularization(self, y_true, y_pred):
        """
        squeezeDet loss function for object detection and classification
        :param y_true: ground truth with shape [batchsize, #anchors, classes+8+labels]
        :param y_pred:
        :return: a tensor of the total loss
        """

        # handle for config
        mc = self.config

        # slice y_true
        input_mask = y_true[:, :, 0]
        input_mask = K.expand_dims(input_mask, axis=-1)
        box_input = y_true[:, :, 1:5]
        box_delta_input = y_true[:, :, 5:9]
        labels = y_true[:, :, 9:]

        # number of objects. Used to normalize bbox and classification loss
        num_objects = K.sum(input_mask)

        # before computing the losses we need to slice the network outputs

        pred_class_probs, pred_conf, pred_box_delta = utils.slice_predictions(y_pred, mc)

        # compute boxes
        det_boxes = utils.boxes_from_deltas(pred_box_delta, mc)

        # again unstack is not avaible in pure keras backend
        unstacked_boxes_pred = []
        unstacked_boxes_input = []

        for i in range(4):
            unstacked_boxes_pred.append(det_boxes[:, :, i])
            unstacked_boxes_input.append(box_input[:, :, i])

        # compute the ious
        ious = utils.tensor_iou(utils.bbox_transform(unstacked_boxes_pred),
                                utils.bbox_transform(unstacked_boxes_input),
                                input_mask,
                                mc)

        # cross-entropy: q * -log(p) + (1-q) * -log(1-p)
        # add a small value into log to prevent blowing up

        # compute class loss
        class_loss = K.sum(labels * (-K.log(pred_class_probs + mc.EPSILON))
                           + (1 - labels) * (-K.log(1 - pred_class_probs + mc.EPSILON))
                           * input_mask * mc.LOSS_COEF_CLASS) / num_objects

        # bounding box loss
        bbox_loss = (K.sum(mc.LOSS_COEF_BBOX * K.square(input_mask * (pred_box_delta - box_delta_input))) / num_objects)

        # reshape input for correct broadcasting
        input_mask = K.reshape(input_mask, [mc.BATCH_SIZE, mc.ANCHORS])

        # confidence score loss
        conf_loss = K.mean(
            K.sum(
                K.square((ious - pred_conf))
                * (input_mask * mc.LOSS_COEF_CONF_POS / num_objects
                   + (1 - input_mask) * mc.LOSS_COEF_CONF_NEG / (mc.ANCHORS - num_objects)),
                axis=[1]
            ),
        )

        # add above losses 
        total_loss = class_loss + conf_loss + bbox_loss

        return total_loss