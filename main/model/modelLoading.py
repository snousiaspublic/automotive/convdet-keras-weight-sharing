# Project: squeezeDetOnKeras
# Filename: modelLoading
# Author: Christopher Ehmann
# Date: 21.12.17
# Organisation: searchInk
# Email: christopher@searchink.com


import h5py
import numpy as np

def load_only_possible_weights(model, weights_file, verbose = False,is_trainable=True,update=True):
    """
    Sets the weights of a model manually by layer name, even if the length of each dimensions does not match.
    :param model: a keras model
    :param weights_file: a keras ckpt file
    """

    #load model file
    f = h5py.File(weights_file, 'r')


    #get list of all datasets that are kernels and biases
    kernels_and_biases_list = []
    def append_name(name):
        if "kernel" in name or "bias" in name:
            kernels_and_biases_list.append(name)
    f.visit(append_name)




    #iterate layers
    for l in model.layers:

        #get the current models weights
        w_and_b = l.get_weights()

        #check for layers that have weights and biases
        if len(w_and_b) == 1:
            for kb in kernels_and_biases_list:

                if l.name + "/kernel" in kb:

                    if verbose:
                        print("Loaded weights for {}".format(l.name))

                    #get shape of both weight tensors
                    model_shape = np.array(w_and_b[0].shape)
                    file_shape =  np.array(f[kb][()].shape)

                    #check for number of axis
                    assert len(model_shape) == len(file_shape)

                    #get the minimum length of each axis
                    min_dims = np.minimum(model_shape, file_shape)

                    #build minimum indices
                    min_idx = tuple(slice(0, x) for x in min_dims)

                    #set to weights of loaded file
                    w_and_b[0][min_idx] = f[kb][()][min_idx]
                    l.set_weights(w_and_b)
                    if not is_trainable==None:
                        l.trainable = is_trainable


                if l.name +":0" == kb:
                    if verbose:
                        print("Loaded biases for {}".format(l.name))

                    fdata= f[kb][()]
                    #get shape of both bias tensors
                    model_shape = np.array(w_and_b[0].shape)
                    file_shape = np.array(f[kb][()].shape)

                    # check for number of axis
                    assert len(model_shape) == len(file_shape)

                    # get the minimum length of each axis
                    min_dims = np.minimum(model_shape, file_shape)

                    #build minimum indices
                    min_idx = tuple(slice(0,x) for x in min_dims)

                    # set to weights of loaded file
                    w_and_b[0][min_idx] = f[kb][()][min_idx]
                    l.set_weights(w_and_b)
                    if not is_trainable == None:
                        l.trainable = is_trainable

        if len(w_and_b) == 2:

            #look for corresponding indices
            for kb in kernels_and_biases_list:
                if l.name + "/kernel" in kb:

                    if verbose:
                        print("Loaded weights for {}".format(l.name))

                    #get shape of both weight tensors
                    model_shape = np.array(w_and_b[0].shape)
                    file_shape =  np.array(f[kb][()].shape)

                    #check for number of axis
                    assert len(model_shape) == len(file_shape)

                    #get the minimum length of each axis
                    min_dims = np.minimum(model_shape, file_shape)

                    #build minimum indices
                    min_idx = tuple(slice(0, x) for x in min_dims)

                    #set to weights of loaded file
                    w_and_b[0][min_idx] = f[kb][()][min_idx]


                if l.name +"/bias" in kb:
                    if verbose:
                        print("Loaded biases for {}".format(l.name))

                    #get shape of both bias tensors
                    model_shape = np.array(w_and_b[1].shape)
                    file_shape = np.array(f[kb][()].shape)

                    # check for number of axis
                    assert len(model_shape) == len(file_shape)

                    # get the minimum length of each axis
                    min_dims = np.minimum(model_shape, file_shape)

                    #build minimum indices
                    min_idx = tuple(slice(0,x) for x in min_dims)

                    # set to weights of loaded file
                    w_and_b[1][min_idx] = f[kb][()][min_idx]



            #update weights
            l.set_weights(w_and_b)
            if not is_trainable == None:
                l.trainable = is_trainable





def load_only_possible_weights_in_pretrained(model, weights_file,weights_file_for_reference, verbose = False,is_trainable=True,update=True):
    """
    Sets the weights of a model manually by layer name, even if the length of each dimensions does not match.
    :param model: a keras model
    :param weights_file: a keras ckpt file
    """

    #load model file
    f = h5py.File(weights_file, 'r')

    r=  h5py.File(weights_file_for_reference, 'r')

    #get list of all datasets that are kernels and biases
    kernels_and_biases_list_target = []

    kernels_and_biases_list_reference= []

    def append_name_target(name):
        if "kernel" in name or "bias" in name:
            kernels_and_biases_list_target.append(name)

    def append_name_reference(name):
        if "kernel" in name or "bias" in name:
            kernels_and_biases_list_reference.append(name)


    f.visit(append_name_target)
    r.visit(append_name_reference)

    #iterate layers
    for l in model.layers:

        #get the current models weights
        w_and_b = l.get_weights()

        #check for layers that have weights and biases
        if len(w_and_b) == 2:

            #look for corresponding indices
            for kb in kernels_and_biases_list_reference:
                if l.name + "/kernel" in kb:

                    if verbose:
                        print("Loaded weights for {}".format(l.name))

                    #get shape of both weight tensors
                    model_shape = np.array(w_and_b[0].shape)
                    file_shape =  np.array(f[kb][()].shape)

                    #check for number of axis
                    assert len(model_shape) == len(file_shape)

                    #get the minimum length of each axis
                    min_dims = np.minimum(model_shape, file_shape)

                    #build minimum indices
                    min_idx = tuple(slice(0, x) for x in min_dims)

                    #set to weights of loaded file
                    w_and_b[0][min_idx] = f[kb][()][min_idx]


                if l.name +"/bias" in kb:
                    if verbose:
                        print("Loaded biases for {}".format(l.name))

                    #get shape of both bias tensors
                    model_shape = np.array(w_and_b[1].shape)
                    file_shape = np.array(f[kb][()].shape)

                    # check for number of axis
                    assert len(model_shape) == len(file_shape)

                    # get the minimum length of each axis
                    min_dims = np.minimum(model_shape, file_shape)

                    #build minimum indices
                    min_idx = tuple(slice(0,x) for x in min_dims)

                    # set to weights of loaded file
                    w_and_b[1][min_idx] = f[kb][()][min_idx]



            #update weights
            l.set_weights(w_and_b)
            l.trainable = is_trainable



def load_only_possible_weights_from_tf_resnet50(model, weights_file, verbose = False,is_trainable=True,update=True):
    """
    Sets the weights of a model manually by layer name, even if the length of each dimensions does not match.
    :param model: a keras model
    :param weights_file: a keras ckpt file
    """

    #load model file
    f = np.load(weights_file,allow_pickle=True)
    fdict=f.item()

    for l in model.layers:
        if l.name=='conv1':
            weights=fdict['conv1']['kernels']
            w_and_b = l.get_weights()
            w_and_b[0]=weights
            l.set_weights(w_and_b)
            if not is_trainable == None:
                l.trainable = is_trainable
            print(l.name)
        if l.name=='conv1/conv1/bias':
            biases = fdict['conv1']['biases']
            w_and_b = l.get_weights()
            w_and_b[0] = biases
            l.set_weights(w_and_b)
            if not is_trainable == None:
                l.trainable = is_trainable
            print(l.name)
        if l.name=='bn_conv1':
            w_and_b = l.get_weights()
            gamma=fdict['conv1']['gamma']
            w_and_b[0]=gamma
            beta=fdict['conv1']['beta']
            w_and_b[1]=beta
            mean=fdict['conv1']['mean']
            w_and_b[2]=mean
            var = fdict['conv1']['var']
            w_and_b[3]=var
            l.set_weights(w_and_b)
            if not is_trainable == None:
                l.trainable = is_trainable
            print(l.name)


        if ('res'in l.name) & ('branch' in l.name):
            for key in fdict:
                if ('res' in key) & ('branch' in key):
                    skey = key[3:]
                    if skey in l.name:
                        print(l.name)
                        reskey='res'+skey
                        weights = fdict[reskey]['kernels']
                        w_and_b = l.get_weights()
                        w_and_b[0] = weights
                        l.set_weights(w_and_b)
                        if not is_trainable == None:
                            l.trainable = is_trainable

        # if ('bn' in l.name) & ('branch' in l.name):
        #     for key in fdict:
        #         if ('res' in key) & ('branch' in key):
        #             skey = key[3:]
        #             if skey in l.name:
        #                 print(l.name)
        #                 # bnkey = 'bn' + skey
        #                 reskey= 'res'+skey
        #
        #                 w_and_b = l.get_weights()
        #
        #                 gamma = fdict[reskey]['gamma']
        #                 w_and_b[0] = gamma
        #                 beta = fdict[reskey]['beta']
        #                 w_and_b[1] = beta
        #                 mean = fdict[reskey]['mean']
        #                 w_and_b[2] = mean
        #                 var = fdict[reskey]['var']
        #                 w_and_b[3] = var
        #                 l.set_weights(w_and_b)
        #                 l.trainable = is_trainable

        if l.name == 'conv12':
                weights = fdict['conv5']['kernels']
                w_and_b = l.get_weights()
                w_and_b[0] = weights
                l.set_weights(w_and_b)
                if not is_trainable == None:
                    l.trainable = is_trainable
                print(l.name)
        if l.name == 'conv12/conv12/bias':
                biases = fdict['conv5']['biases']
                w_and_b = l.get_weights()
                w_and_b[0] = biases
                l.set_weights(w_and_b)
                if not is_trainable == None:
                    l.trainable = is_trainable
                print(l.name)


























    # #iterate layers
    # for l in model.layers:
    #
    #     #get the current models weights
    #     w_and_b = l.get_weights()
    #
    #     #check for layers that have weights and biases
    #     if len(w_and_b) == 1:
    #         for kb in kernels_and_biases_list:
    #
    #             if l.name + "/kernel" in kb:
    #
    #                 if verbose:
    #                     print("Loaded weights for {}".format(l.name))
    #
    #                 #get shape of both weight tensors
    #                 model_shape = np.array(w_and_b[0].shape)
    #                 file_shape =  np.array(f[kb][()].shape)
    #
    #                 #check for number of axis
    #                 assert len(model_shape) == len(file_shape)
    #
    #                 #get the minimum length of each axis
    #                 min_dims = np.minimum(model_shape, file_shape)
    #
    #                 #build minimum indices
    #                 min_idx = tuple(slice(0, x) for x in min_dims)
    #
    #                 #set to weights of loaded file
    #                 w_and_b[0][min_idx] = f[kb][()][min_idx]
    #                 l.set_weights(w_and_b)
    #                 l.trainable = is_trainable
    #
    #
    #             if l.name +":0" == kb:
    #                 if verbose:
    #                     print("Loaded biases for {}".format(l.name))
    #
    #                 fdata= f[kb][()]
    #                 #get shape of both bias tensors
    #                 model_shape = np.array(w_and_b[0].shape)
    #                 file_shape = np.array(f[kb][()].shape)
    #
    #                 # check for number of axis
    #                 assert len(model_shape) == len(file_shape)
    #
    #                 # get the minimum length of each axis
    #                 min_dims = np.minimum(model_shape, file_shape)
    #
    #                 #build minimum indices
    #                 min_idx = tuple(slice(0,x) for x in min_dims)
    #
    #                 # set to weights of loaded file
    #                 w_and_b[0][min_idx] = f[kb][()][min_idx]
    #                 l.set_weights(w_and_b)
    #                 l.trainable = is_trainable
    #
    #     if len(w_and_b) == 2:
    #
    #         #look for corresponding indices
    #         for kb in kernels_and_biases_list:
    #             if l.name + "/kernel" in kb:
    #
    #                 if verbose:
    #                     print("Loaded weights for {}".format(l.name))
    #
    #                 #get shape of both weight tensors
    #                 model_shape = np.array(w_and_b[0].shape)
    #                 file_shape =  np.array(f[kb][()].shape)
    #
    #                 #check for number of axis
    #                 assert len(model_shape) == len(file_shape)
    #
    #                 #get the minimum length of each axis
    #                 min_dims = np.minimum(model_shape, file_shape)
    #
    #                 #build minimum indices
    #                 min_idx = tuple(slice(0, x) for x in min_dims)
    #
    #                 #set to weights of loaded file
    #                 w_and_b[0][min_idx] = f[kb][()][min_idx]
    #
    #
    #             if l.name +"/bias" in kb:
    #                 if verbose:
    #                     print("Loaded biases for {}".format(l.name))
    #
    #                 #get shape of both bias tensors
    #                 model_shape = np.array(w_and_b[1].shape)
    #                 file_shape = np.array(f[kb][()].shape)
    #
    #                 # check for number of axis
    #                 assert len(model_shape) == len(file_shape)
    #
    #                 # get the minimum length of each axis
    #                 min_dims = np.minimum(model_shape, file_shape)
    #
    #                 #build minimum indices
    #                 min_idx = tuple(slice(0,x) for x in min_dims)
    #
    #                 # set to weights of loaded file
    #                 w_and_b[1][min_idx] = f[kb][()][min_idx]
    #
    #
    #
    #         #update weights
    #         l.set_weights(w_and_b)
    #         l.trainable = is_trainable

