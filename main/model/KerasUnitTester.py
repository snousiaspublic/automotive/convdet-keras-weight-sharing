import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Embedding
from keras.layers import Convolution1D, MaxPooling1D, Convolution2D, MaxPooling2D
from keras import backend as K
from keras.layers.convolutional import ZeroPadding2D
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# FIT A SIMPLE MODEL

N = 50
X = np.random.randn(N, 3, 5, 5)  # creates the 3 channel data, 5x5 matrices
y = np.random.randint(1, size=N)

model = Sequential()
model.add(Convolution2D(filters=2,kernel_size=(3,3),padding='valid',strides=(1,1)))

model.add(Activation('relu'))

model.add(MaxPooling2D(pool_size=(n_pool, n_pool)))

# flatten the data for the 1D layers
model.add(Flatten())

# Dense(n_outputs)
model.add(Dense(10))

# the softmax output layer gives us a probablity for each class
model.add(Dense(1))
model.add(Activation('linear'))

model.compile(loss='mse',
              optimizer='adam',
              metrics=['accuracy'])

print(model.summary())

# how many examples to look at during each training iteration
batch_size = 1

# how many times to run through the full set of examples
n_epochs = 1

model.fit(X,
          y,
          batch_size=batch_size,
          nb_epoch=n_epochs)
