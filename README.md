# Convdets on Keras for Object Detection for Autonomous Driving with weight sharing evaluation

Stavros Nousias, Erion-Vasilis Pikoulis, Christos Mavrokefalidis, Aris S. Lalos

This repository evaluates the effect of weight sharing on Squeezedet and ResNet50Convdet with respect to object detection in autonomous driving
        
    @inproceedings{Pikoulis2020,
        author = {Pikoulis, Erion Vasilis and Mavrokefalidis, Christos and Lalos, Aris S},
        booktitle = {International Conference on Machine Learning and Applications},
        title = {{A New Clustering-Based Technique for the Acceleration of Deep Convolutional Networks}},
        year = {2020}
    }




## Version

This repository is a beta testing version. 
        
## Disclaimer
The base code for this repository is the Keras implementation of SqueezeDet by Bichen Wu, Alvin Wan, Forrest Iandola, Peter H. Jin, Kurt Keutzer (UC Berkeley & DeepScale)
Squeezedet is a convolutional neural network based object detector described in this paper: https://arxiv.org/abs/1612.01051. The original implementation can be found [here](https://github.com/BichenWuUCB/squeezeDet).

    @inproceedings{squeezedet,
        Author = {Bichen Wu and Forrest Iandola and Peter H. Jin and Kurt Keutzer},
        Title = {SqueezeDet: Unified, Small, Low Power Fully Convolutional Neural Networks for Real-Time Object Detection for Autonomous Driving},
        Journal = {arXiv:1612.01051},
        Year = {2016}
    }




### Execution ###

* Download the KITTI 2D object detection training dataset

        http://www.cvlibs.net/download.php?file=data_object_image_2.zip

        http://www.cvlibs.net/download.php?file=data_object_label_2.zip

* Unzip the dataset. 

* Create a training test split

* Train 
 
        python scripts/train_main_routine.py

* Apply weight sharing. Optimize each layer and retrain the following with simultaneous evaluation

        python train_biases_recursive_weight_sharing_v4.py
        

### Tensorboard visualization

For tensoboard visualization you can can run:


    tensorboard --logdir log

Open in your brower  **localhost:6006** or the IP where you ran the training. 


