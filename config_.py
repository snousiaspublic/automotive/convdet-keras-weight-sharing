root = "/home/remoteuser/Workspace/squeezedet-keras/"
root_dataset = root + "data/"
root="E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/"
root_dataset="E:/Dropbox/_GroundWork/_Datasets/"

img_file = root_dataset + "KITTI/ImageSets/img_train.txt"
gt_file = root_dataset + "KITTI/ImageSets/gt_train.txt"


network='squeezedet'
# network='resnet50convdetv2'
mode='train'


if mode=='train':
    EPOCHS = 300
    STEPS = None
    OPTIMIZER = None
    CUDA_VISIBLE_DEVICES = "0"
    GPUS = 1
    PRINT_TIME = 0
    REDUCELRONPLATEAU = True
    VERBOSE=True
    nbatchmul=1


# Otpimization
if mode=='optimize':
    EPOCHS = 100
    STEPS = None
    OPTIMIZER = None
    CUDA_VISIBLE_DEVICES = "0"
    GPUS = 1
    PRINT_TIME = 0
    REDUCELRONPLATEAU = True
    VERBOSE = True
    reTrainingEpochs = 15
    nbatchmul = 1
    reTrainingSteps = None
    method = 'vq'
    acceleration = 10

if mode=='eval':
    STEPS = None