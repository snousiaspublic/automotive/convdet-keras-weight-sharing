import argparse
import os
import gc
import numpy as np
import glob
import os
import tensorflow as tf
# Load networks
from main.model.squeezeDet import  SqueezeDet
from main.model.resnet50DetV2 import Resnet50DetV2
from keras.layers import Layer
from main.model.dataGenerator import generator_from_data_path
from keras.models import load_model
import keras.backend as K
from keras import optimizers
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau,TerminateOnNaN
from keras.callbacks import Callback
from keras.utils import multi_gpu_model
import time
from main.model.modelLoading import load_only_possible_weights,load_only_possible_weights_in_pretrained,load_only_possible_weights_from_tf_resnet50
from main.model.multi_gpu_model_checkpoint import  ModelCheckpointMultiGPU
import pickle
from main.config.create_config import load_dict


from main.model.squeezeDet import SqueezeDet
from main.model.resnet50DetV2 import Resnet50DetV2
# from main.model.VGG16DetBiasV2 import VGG16DetBiasV2
from main.model.dataGenerator import generator_from_data_path
from keras.models import load_model
import keras.backend as K
from keras import optimizers
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from keras.utils import multi_gpu_model
import time
from main.model.modelLoading import load_only_possible_weights, load_only_possible_weights_in_pretrained
from main.model.multi_gpu_model_checkpoint import ModelCheckpointMultiGPU
import argparse
import os
import gc
import numpy as np
import pickle
from main.config.create_config import load_dict
from main.utils.weight_sharing import quant_convlayer_weights
from main.model.evaluation import evaluate
from keras.callbacks import TerminateOnNaN
import warnings
import glob
from keras.callbacks import Callback
from main.utils.adabound import AdaBound
from main.utils.AdamW import AdamW
from main.utils.findBatchSize import FindBatchSize,get_model_memory_usage


from main.model.squeezeDet import  SqueezeDet
from main.model.resnet50DetV2 import Resnet50DetV2
# from main.model.VGG16DetBiasV0 import VGG16DetBiasV0
# from main.model.VGG16DetBiasV1 import VGG16DetBiasV1
# from main.model.VGG16DetBiasV2 import VGG16DetBiasV2

from main.model.dataGenerator import generator_from_data_path, visualization_generator_from_data_path
import keras.backend as K
from keras import optimizers
import tensorflow as tf
from main.model.evaluation import evaluate
from main.model.visualization import  visualize
import os
import time
import numpy as np
import argparse
from keras.utils import multi_gpu_model
from main.config.create_config import load_dict
import cv2
from main.utils.weight_sharing import quant_convlayer_weights


class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='val_loss', value=0.00001, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)
        if current < self.value:
            if self.verbose > 0:
                print("Epoch %05d: early stopping THR" % epoch)
            self.model.stop_training = True